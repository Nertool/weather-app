import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import {weatherOptions} from './const';

export default function Weather({temp, condition}) {
	return (
		<LinearGradient colors={weatherOptions[condition].gradient} style={styles.container}>
			<StatusBar barStyle="light-content" />
			<View style={styles.container}>
				<MaterialCommunityIcons name={weatherOptions[condition].iconName} size={90} color={'white'}/>
				<Text style={styles.text}>{temp}°</Text>
			</View>
			<View style={{...styles.container, ...styles.textContainer}}>
				<Text style={styles.title}>{weatherOptions[condition].title}</Text>
				<Text style={styles.subtitle}>{weatherOptions[condition].subtitle}</Text>
			</View>
		</LinearGradient>
	);
}

Weather.propTypes = {
	temp: PropTypes.number.isRequired,
	condition: PropTypes.oneOf(['Thunderstorm', 'Drizzle', 'Rain', 'Snow', 'Dust', 'Mist', 'Smoke', 'Clear', 'Clouds']).isRequired
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		fontSize: 42,
		color: 'white'
	},
	title: {
		color: 'white',
		fontSize: 44,
		fontWeight: '300',
		marginBottom: 10,
		textAlign: 'left'
	},
	subtitle: {
		color: 'white',
		fontSize: 24,
		fontWeight: '600',
		textAlign: 'left'
	},
	textContainer: {
		paddingHorizontal: 40,
		alignItems: 'flex-start',
		flex: 1,
		justifyContent: 'center'
	}
});
